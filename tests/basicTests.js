
let baseUrl = "http://localhost:3000";

module.exports = {

    tags: ['Testing Hospital Simulator Syone'],

    'HSS HTML elements test'(browser){
        browser
            .url(baseUrl)
            .waitForElementVisible('nav', 1000)
            .assert.title('Hospital Simulator - Syone')
            .assert.visible('#feverInput')
            .assert.visible('#healthyInput')
            .assert.visible('#diabetesInput')
            .assert.visible('#tuberculosisInput')
            .assert.visible('#deadInput')
            .assert.visible('#aspirinInput')
            .assert.visible('#antibioticInput')
            .assert.visible('#insulinInput')
            .assert.visible('#paracetamolInput')
            .assert.visible('#probability')
            .assert.visible('#resultsTable')
            .assert.elementNotPresent('#resurrectionMessage')
            .setValue('#deadInput', 100)
            .pause(1000)
            .clearValue('input[id=probability]')
            .setValue('input[id=probability]', 1)
            .pause(1000)
            .assert.elementPresent('#resurrectionMessage')
    },

    'Basic HSS mechanics test'(browser){
        browser
            .url(baseUrl)
            .waitForElementVisible('nav', 1000)
            .assert.containsText('#feverResultValue', 0)
            .assert.containsText('#healthyResultValue', 0)
            .setValue('#feverInput', 100)
            .pause(1000)
            .assert.containsText('#feverResultValue', 100)
            .assert.containsText('#healthyResultValue', 0)
            .click('#aspirinInput')
            .pause(1000)
            .assert.containsText('#feverResultValue', 0)
            .assert.containsText('#healthyResultValue', 100)
    }
}