import React from 'react';

import { HospitalSimHeader } from "./components/HospitalSimHeader"
import { HospitalSimBody } from "./components/HospitalSimBody"

const App: React.FC = () => {
  return (
      <React.Fragment>
        <HospitalSimHeader/>
        <HospitalSimBody/>
      </React.Fragment>
  );
}

export default App;
