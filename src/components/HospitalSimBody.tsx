import React from 'react'
import { Container, Col, Row, Form,
    FormControlProps, ToggleButton,
    ToggleButtonGroup } from 'react-bootstrap'

import {
    IHSSState, IHSSTextBoxChangeHandler,
    IHSSButtonToggleChangeHandler,
    IHSSPatientsState, IHSSDrugsState, IHSSSimulationResult
} from './helpers/HSSInterfaces'
import { sectionHeader, capitaliseString } from './helpers/HSSUtils'
import calculateResult from './helpers/HSSResultAlgorithm'
import { resultTable, resurrectionMessage } from './helpers/HSSResultRenderFunctions'

// Main React Component (no props needed)
export class HospitalSimBody extends React.PureComponent<{}, IHSSState> {

    // State of this React Component (App)
    state:IHSSState = {
        patients: {
            descriptions:[ // Different diagnoses a patient can have
                'fever', 'healthy',
                'diabetes', 'tuberculosis',
                'dead'
            ],
            before: [ 0, 0, 0, 0, 0 ], // Text boxes input values
            after: [ 0, 0, 0, 0, 0 ], // Result table values
            resurrectionProbability: 1000000, // Probability of someone being resurrected
            resurrected: 0 // Number of resurrected in last simulation
        },
        drugs: {
            descriptions: [ // Different drugs a patient can have
                'aspirin',
                'antibiotic',
                'insulin',
                'paracetamol'
            ],
            used: [] // Drugs used on the patients
        }
    }

    /////////////////////   INPUT PATIENTS SECTION   /////////////////////

    // Input section to specify the number of patients per diagnosis
    inputSectionPatients(): JSX.Element[]{

        const patients: IHSSPatientsState = this.state.patients

        return patients.descriptions.map( // Create an input section per diagnosis
            (description:string, index:number) =>
                (
                    <Form.Group as={ Col } controlId={ description + 'Input' } key={ description + 'Input' }>
                        <Form.Label> { capitaliseString( description ) } </Form.Label>
                        <Form.Control
                            placeholder="Number of Patients"
                            type="number"
                            value={ String( patients.before[index] ) }
                            onChange={this.handleInputChangePatients(index)}
                            maxLength={7}
                        />
                    </Form.Group>
                )
        )
    }

    // Function that deals with change in the number of patients per diagnosis text boxes
    handleInputChangePatients(index:number): IHSSTextBoxChangeHandler {
        return (event: React.FormEvent<FormControlProps>) => {

            // Collects new integer in text box (validates input: integer, 0 or over)
            let newValue: number = parseInt( String( event.currentTarget.value ) )
            newValue = isNaN(newValue) || newValue < 0 ? 0 : newValue

            // Creates new value array for the input text boxes
            let newBefore: number[] = [...this.state.patients.before]
            newBefore[index] = newValue

            // Sets the array in this React Component's state
            this.setState({
                ...this.state,
                patients: {
                    ...this.state.patients,
                    before: newBefore
                }
            })
        }
    }

    /////////////////////   INPUT DRUGS SECTION   /////////////////////

    // Button section to specify the drugs used to medicate all patients
    inputSectionDrugs(): JSX.Element{

        const drugs: IHSSDrugsState = this.state.drugs

        return ( // Create a toggle input section per drug
            <ToggleButtonGroup type="checkbox" value={this.state.drugs.used}
                               onChange={this.handleInputChangeDrugs()}>
                {
                    drugs.descriptions.map((description:string) => (
                            <ToggleButton value={description}
                                          key={ description + 'Input' } id={ description + 'Input' }>
                                { capitaliseString( description ) }
                            </ToggleButton>
                        )
                    )
                }
            </ToggleButtonGroup>
        )
    }

    // Function that deals with change in the drugs used buttons
    handleInputChangeDrugs(): IHSSButtonToggleChangeHandler{
        return ( event:string[] ) => {

            // Sets the array in this React Component's state
            this.setState({
                ...this.state,
                drugs: {
                    ...this.state.drugs,
                    used: event
                }
            })
        }
    }

    /////////////////////   INPUT RESURRECTION SECTION   /////////////////////

    // Input section to specify the patient resurrection probability
    inputSectionResurrection(): JSX.Element{

        const probability: number = this.state.patients.resurrectionProbability

        return (
            <Row className="justify-content-md-center">
                The chances of the Flying Spaghetti Monster saving someone are 1 in
                <Form.Control
                    placeholder="1 in how many"
                    id="probability"
                    type="number"
                    min={1}
                    value={ String( probability ) }
                    onChange={this.handleInputChangeResurrection()}
                    maxLength={7}
                    />
            </Row>
        )
    }

    // Function that deals with change in the resurrection probability text boxes
    handleInputChangeResurrection(): IHSSTextBoxChangeHandler{
        return (event: React.FormEvent<FormControlProps>) => {

            // Collects new integer in text box (validates input: integer, 1 or over)
            let newValue: number = parseInt( String( event.currentTarget.value ) )
            newValue = isNaN(newValue) || newValue < 1 ? 1 : newValue

            // Sets the probabilistic value in this React Component's state
            this.setState({
                ...this.state,
                patients: {
                    ...this.state.patients,
                    resurrectionProbability: newValue
                }
            })
        }
    }

    /////////////////////   RESULT CALCULATION SECTION   /////////////////////

    // Calculate the results of the simulation
    calculateAndSetResult(): void{

        // Calculate and get results
        let result: IHSSSimulationResult = calculateResult(this.state)

        // Sets the result values in this React Component's state
        this.setState({
            ...this.state,
            patients: {
                ...this.state.patients,
                after: result.after,
                resurrected: result.resurrected
            }
        })
    }

    /////////////////////   REACT COMPONENT LIFECYCLE FUNCTIONS   /////////////////////

    // Recalculates results if any of the inputs change (called with every state change)
    componentDidUpdate(prevProps:{}, prevState: IHSSState, snapshot:{}): void{
        if(this.state.patients.before !== prevState.patients.before
            || this.state.drugs.used !== prevState.drugs.used
            || this.state.patients.resurrectionProbability !== prevState.patients.resurrectionProbability) {

                this.calculateAndSetResult()
        }
    }

    // Used to render the App's body (called with every state change)
    render(): JSX.Element{
        return (
            <Container>
                <Form>
                    {/* Patients Input section */}
                    {sectionHeader('Insert Number of Patients with each diagnosis:')}

                    <Form.Row className="list-group-item-info">
                        {this.inputSectionPatients()}
                    </Form.Row>

                    {/* Drugs Input button section */}
                    {sectionHeader('Choose drugs to medicate patients:')}

                    <Form.Row className="justify-content-md-center">
                        {this.inputSectionDrugs()}
                    </Form.Row>

                    <br/>

                    {/* Resurrection probability Input section */}
                    <Form.Row className="justify-content-md-center">
                        {this.inputSectionResurrection()}
                    </Form.Row>

                    <hr style={{ color: "red", backgroundColor: "red", height: 5}} />

                    {/* Calculated result table section */}
                    {sectionHeader('The results are in:')}

                    <Form.Row className="justify-content-md-center">
                        {resultTable(this.state)}
                    </Form.Row>

                    {/* Resurrection feedback (if any) */}
                    <Form.Row className="justify-content-md-center">
                        {resurrectionMessage(this.state)}
                    </Form.Row>
                </Form>
            </Container>
        )
    }
}
