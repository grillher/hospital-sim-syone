import React from 'react'
import {Navbar, Nav, Container} from 'react-bootstrap'

export function HospitalSimHeader(): JSX.Element{
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>

                {/* Website title */}
                <Navbar.Brand>Hospital Simulator</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                    </Nav>

                    {/* Link to Syone's website */}
                    <Nav>
                        <Nav.Link href="https://www.syone.com/"  target="_blank">Go to Syone</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
