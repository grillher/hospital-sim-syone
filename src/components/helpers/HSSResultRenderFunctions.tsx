import {Alert, Table} from "react-bootstrap"
import {capitaliseString, colorByDiagnosis} from "./HSSUtils"
import React from "react"

import {IHSSPatientsState, IHSSState} from "./HSSInterfaces";

// Result section that shows the number of Patients per diagnosis after the simulation
export function resultTable(state: IHSSState): JSX.Element{

    const patients: IHSSPatientsState = state.patients

    return (
        <Table striped bordered hover id="resultsTable">
            <thead>
            <tr>
                { // Prints the diagnosis descriptions on the result table header
                    patients.descriptions.map(
                        (description: string) => (
                            <th className={colorByDiagnosis(description)} key={ description + 'ResultTitle' }>
                                { capitaliseString( description ) }
                            </th>
                        )
                    )
                }
            </tr>
            </thead>
            <tbody>
            <tr>
                { // Prints the simulation result value on the result table body
                    patients.descriptions.map(
                        (description: string, index: number) => (
                            <td className={colorByDiagnosis(description)}
                                id={ description + 'ResultValue' }
                                key={ description + 'ResultValue' }>
                                { patients.after[index] }
                            </td>
                        )
                    )
                }
            </tr>
            </tbody>
        </Table>
    )
}

// Feedback section that tells you if the Flying Spaghetti Monster showed up
export function resurrectionMessage(state: IHSSState): JSX.Element {
    const resurrected: number = state.patients.resurrected

    return (
        resurrected > 0
            ? <Alert variant="warning" id="resurrectionMessage">
                {'Woohoo! The Flying Spaghetti Monster just saved '
                + (resurrected > 1
                    ? resurrected + ' lives 🙌'
                    : 'a life 🙌')}
            </Alert>
            : <React.Fragment></React.Fragment>

    )
}