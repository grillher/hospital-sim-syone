import {Form} from "react-bootstrap"
import React from "react"

// The JSX block used to place a section header
export function sectionHeader(text:string): JSX.Element{
    return(
        <Form.Row className="justify-content-md-center">
            <Form.Label>
                <br/>
                <h3>
                    {text}
                </h3>
                <br/>
            </Form.Label>
        </Form.Row>
    )
}


// Specifies what CSS className to use in results per diagnosis
export function colorByDiagnosis(description: string): string{
    let ret: string = ''

    switch(description){
        case 'dead':
            ret = 'list-group-item-danger'
            break
        case 'healthy':
            ret = 'list-group-item-success'
            break
        default:
            break
    }

    return ret
}

// Capitalises the first letter of a string
export function capitaliseString(text:string): string{
    return text.charAt(0).toUpperCase() + text.slice(1)
}
