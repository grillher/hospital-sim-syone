import React from "react"
import {FormControlProps} from "react-bootstrap"

export interface IHSSState {
    patients: IHSSPatientsState
    drugs: IHSSDrugsState
}

export interface IHSSPatientsState {
    descriptions: string[]
    before: number[]
    after: number[]
    resurrectionProbability: number
    resurrected: number
}

export interface IHSSDrugsState {
    descriptions: string[]
    used: string[]
}

export interface IHSSTextBoxChangeHandler {
    (event: React.FormEvent<FormControlProps>): void
}

export interface IHSSButtonToggleChangeHandler {
    (event: string[]): void
}

export interface IHSSSimulationResult {
    after: number[]
    resurrected: number
}
