import {IHSSSimulationResult, IHSSState} from "./HSSInterfaces"

// Calculate the results of the simulation
export default function calculateResult(state: IHSSState): IHSSSimulationResult{

    // Values to calculate
    let result: number[] = [...state.patients.before]
    let resurrected: number = 0

    // Flags for post-calculation of drug combination results
    let gaveAspirin: boolean = false
    let gaveAntibiotic: boolean = false
    let gaveInsulin: boolean = false
    let gaveParacetamol: boolean = false

    // Useful data to calculate result
    const patientsDescriptions: string[] = state.patients.descriptions
    const healthyIndex: number = patientsDescriptions.indexOf('healthy')
    const deadIndex: number = patientsDescriptions.indexOf('dead')

    for(let d of state.drugs.used){
        switch(d){
            case 'aspirin':
                // Need to know if it's being used with paracetamol
                // and if there will be any more fever patients before curing them
                gaveAspirin = true
                break
            case 'antibiotic':
                gaveAntibiotic = true

                // Antibiotic cures tuberculosis
                const tuberculosisIndex: number = patientsDescriptions.indexOf('tuberculosis')
                result[healthyIndex] += result[tuberculosisIndex]
                result[tuberculosisIndex] = 0

                break
            case 'insulin':
                // If you don't get this flag by the end, diabetes patients die.
                // Need to know if it's being used with antibiotic (the healthy get a fever)
                gaveInsulin = true
                break
            case 'paracetamol':
                // Need to know if it's being used with aspirin
                // and if there will be any more fever patients before curing them
                gaveParacetamol = true
                break
            default:
                break
        }
    }

    // Paracetamol with Aspirin kills all
    if(gaveParacetamol && gaveAspirin){
        const total: number = result.reduce((a,b) => a + b, 0)
        result = [0, 0, 0, 0, 0]
        result[deadIndex] = total
    }
    else{

        // No insulin kills diabetic patients
        if(!gaveInsulin){
            const diabetesIndex: number = patientsDescriptions.indexOf('diabetes')
            result[deadIndex] += result[diabetesIndex]
            result[diabetesIndex] = 0
        }
        // Insulin with Antibiotic makes healthy people catch a fever
        if(gaveInsulin && gaveAntibiotic){
            const feverIndex: number = patientsDescriptions.indexOf('fever')
            result[feverIndex] += result[healthyIndex]
            result[healthyIndex] = 0
        }
        // Aspirin and Paracetamol cures fever
        if(gaveAspirin || gaveParacetamol){
            const feverIndex: number = patientsDescriptions.indexOf('fever')
            result[healthyIndex] += result[feverIndex]
            result[feverIndex] = 0
        }
    }

    // Flying spaghetti Monster resurrection
    const resurrectionProbability: number = state.patients.resurrectionProbability

    // Calculated if each dead patient is resurrect, given the probability
    for(let i = result[deadIndex]; i > 0; i--){
        const randVal: number = Math.floor( Math.random() * resurrectionProbability )

        if( randVal === 0 ){
            result[deadIndex]--
            result[healthyIndex]++
            resurrected++
        }
    }

    // Sets the result values in this React Component's state
    return {after: result, resurrected: resurrected}
}